﻿using System;
using static System.Math;

namespace PriorityQueue
{
    public class PriorityQueue<TValue, TPriority> 
        where TPriority : IComparable<TPriority>
    {
        private Node _root;

        public int Count { get; private set; }

        public Node Enqueue(TValue value, TPriority priority)  
        {
            var node = CreateNode(value, priority);
            AddNodeToRootList(ref node, ref _root);
            if (_root == null || node.Priority.CompareTo(_root.Priority) > 0)
            {
                _root = node;
            }
            Count++;
            return node;
        }

        public TValue Dequeue()
        {
            return RemoveMax().Value;
        }

        public TValue Peek()
        {
            return _root.Value;
        }

        public void IncreasePriority(Node node, TPriority newPriority)
        {
            if (newPriority.CompareTo(node.Priority) < 0)
            {
                return;
            }
            node.Priority = newPriority;
            var temp = node.Parent;
            if (temp != null && node.Priority.CompareTo(temp.Priority) > 0)
            {
                Cut(_root, temp, node);
                CascadingCut(_root, temp);
            }
            if (node.Priority.CompareTo(_root.Priority) > 0)
            {
                _root = node;
            }
        }

        public void Merge(PriorityQueue<TValue, TPriority> other)
        {
            LinkRootLists(_root, other._root);
            if (_root == null || other._root != null && other._root.Priority.CompareTo(_root.Priority) > 0)
            {
                _root = other._root;
            }
            Count += other.Count;
        }

        private static Node CreateNode(TValue value, TPriority priority)
        {
            var node = new Node
            {
                Priority = priority,
                Value = value,
                Degree = 0,
                Mark = false,
                Parent = null,
                Сhild = null
            };
            node.Left = node;
            node.Right = node;
            return node;
        }

        private void Consolidate()
        {
            var lenght = (int)Ceiling(Log(Count, 2));
            var array = new Node[lenght];
            var node = _root;
            do
            {
                var x = node;
                var degree = x.Degree;
                while (array[degree] != null)
                {
                    var y = array[degree];
                    if (x.Priority.CompareTo(y.Priority) < 0)
                    {
                        Swap(ref x, ref y);
                    }
                    Link(ref y, ref x);
                    array[degree] = null;
                    degree++;
                }
                array[degree] = x;
                node = node.Left;
            } while (node != _root);
            _root = null;
            for (var index = 0; index < lenght; index++)
            {
                if (array[index] != null)
                {
                    AddNodeToRootList(ref array[index], ref _root);
                    if (_root == null || array[index].Priority.CompareTo(_root.Priority) > 0)
                    {
                        _root = array[index];
                    }
                }
            }
        }

        private static void LinkRootLists(Node first, Node second)
        {
            if (first == null || second == null)
            {
                return;
            }
            var firstLeft = first.Left;
            var secondLeft = second.Left;
            firstLeft.Right = second;
            second.Left = firstLeft;
            first.Left = secondLeft;
            secondLeft.Right = first;
        }

        private void Link(ref Node first, ref Node second)
        {
            //if (first == null)
            //{
            //    return;
            //}
            second.Degree++;
            RemoveNodeFromRootList(ref first, ref _root);
            first.Parent = second;
            if (second.Сhild != null)
            {
                AddNodeToRootList(ref first, ref second.Сhild);
            }
            else
            {
                second.Сhild = CreateNode(first.Value, first.Priority);
            }
            first.Mark = false;
        }

        private static void Swap(ref Node first, ref Node second)
        {
            var temp = first;
            first = second;
            second = temp;
        }

        private Node RemoveMax()
        {
            var result = _root;
            if (result == null)
            {
                return null;
            }
            var node = _root.Сhild;
            while (node != null)
            {
                AddNodeToRootList(ref node, ref _root);
                node.Parent = null;
                node = node.Сhild;
            }
            RemoveNodeFromRootList(ref result, ref _root);
            if (result == result.Right)
            {
                _root = null;
            }
            else
            {
                _root = result.Right;
                Consolidate();
            }
            Count--;
            return result;
        }

        private static void RemoveNodeFromRootList(ref Node node, ref Node heap)
        {
            if (heap == null)
            {
                return;
            }
            if (node.Left == node)
            {
                heap = null;
                return;
            }
            var left = node.Left;
            var right = node.Right;
            left.Right = node.Right;
            right.Left = node.Left;
            node.Left = node;
            node.Right = node;
        }

        private static void Cut(Node heap, Node node, Node cutedNode)
        {
            RemoveNodeFromRootList(ref cutedNode, ref node.Сhild);
            node.Degree--;
            AddNodeToRootList(ref cutedNode, ref heap);
            cutedNode.Parent = null;
            cutedNode.Mark = false;
        }

        private static void CascadingCut(Node heap, Node node)
        {
            while (true)
            {
                var temp = node.Parent;
                if (temp == null)
                {
                    return;
                }
                if (node.Mark == false)
                {
                    node.Mark = true;
                    return;
                }
                Cut(heap, temp, node);
                node = temp;
            }
        }

        private static void AddNodeToRootList(ref Node node, ref Node heap)
        {
            if (heap == null)
            {
                return;
            }
            if (heap.Left == heap)
            {
                heap.Left = node;
                heap.Right = node;
                node.Left = heap;
                node.Right = heap;
                return;
            }
            var leftNode = heap.Left;
            heap.Left = node;
            node.Right = heap;
            node.Left = leftNode;
            leftNode.Right = node;
        }

        public class Node
        {
            internal TPriority Priority;
            internal TValue Value;
            internal Node Parent;
            internal Node Сhild;
            internal Node Left;
            internal Node Right;
            internal int Degree;
            internal bool Mark;
        }
    }
}