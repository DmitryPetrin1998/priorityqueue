﻿using NUnit.Framework;

namespace PriorityQueue.Tests
{
    [TestFixture]
    public class PriorityQueueTestFixture
    {
        //[Test]
        //public void Priority_queue_enqueue_can_be_executed()
        //{
        //    var priorityQueue = new PriorityQueue<int, int>();

        //    priorityQueue.Enqueue(25, 10);
        //    Assert.AreEqual(25, priorityQueue.Peek());

        //    priorityQueue.Enqueue(55, 13);
        //    Assert.AreEqual(55, priorityQueue.Peek());

        //    priorityQueue.Enqueue(0, 20);
        //    Assert.AreEqual(0, priorityQueue.Peek());

        //    priorityQueue.Enqueue(45, 12);
        //    Assert.AreEqual(0, priorityQueue.Peek());
        //}

        [Test]
        public void Priority_queue_dequeue_can_be_executed()
        {
            var priorityQueue = new PriorityQueue<int, int>();
            priorityQueue.Enqueue(55, 13);
            priorityQueue.Enqueue(25, 10);
            priorityQueue.Enqueue(40, 23);
            priorityQueue.Enqueue(27, 10);

            Assert.AreEqual(40, priorityQueue.Dequeue());
            Assert.AreEqual(55, priorityQueue.Peek());
            Assert.AreEqual(55, priorityQueue.Dequeue());
            Assert.AreEqual(27, priorityQueue.Peek());
            Assert.AreEqual(27, priorityQueue.Dequeue());
            Assert.AreEqual(25, priorityQueue.Peek());
            Assert.AreEqual(25, priorityQueue.Dequeue());
        }

        //[Test]
        //public void Priority_queue_merge_can_be_executed()
        //{
        //    var firstPriorityQueue = new PriorityQueue<int, int>();
        //    firstPriorityQueue.Enqueue(55, 13);
        //    firstPriorityQueue.Enqueue(25, 10);

        //    var secondPriorityQueue = new PriorityQueue<int, int>();
        //    secondPriorityQueue.Enqueue(40, 23);
        //    secondPriorityQueue.Enqueue(27, 10);

        //    firstPriorityQueue.Merge(secondPriorityQueue);

        //    Assert.AreEqual(40, firstPriorityQueue.Dequeue());
        //    Assert.AreEqual(55, firstPriorityQueue.Peek());
        //    Assert.AreEqual(55, firstPriorityQueue.Dequeue());
        //    Assert.AreEqual(27, firstPriorityQueue.Peek());
        //    Assert.AreEqual(27, firstPriorityQueue.Dequeue());
        //    Assert.AreEqual(25, firstPriorityQueue.Peek());
        //    Assert.AreEqual(25, firstPriorityQueue.Dequeue());
        //}

        //[Test]
        //public void Priority_queue_increase_priority_can_be_executed()
        //{
        //    var priorityQueue = new PriorityQueue<int, int>();
        //    priorityQueue.Enqueue(55, 13);
        //    priorityQueue.Enqueue(30, 10);
        //    var node = priorityQueue.Enqueue(25, 11);
        //    priorityQueue.Enqueue(20, 8);

        //    priorityQueue.IncreasePriority(node, 15);

        //    Assert.AreEqual(15, node.Priority);
        //    Assert.AreEqual(25, priorityQueue.Peek());
        //}
    }
}